package ru.smlab.selenium.util;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.smlab.selenium.pages.BasePage;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

//Pattern Singleton - 1 exemplar of class
public class WDWrapper implements WebDriver {

    //единственный экземпляр
    private static WDWrapper wd;

    public static Logger LOG = LogManager.getLogger(BasePage.class);

    //ленивая инициализация
    public static WDWrapper getInstance() {
        if (wd == null) {
            wd = new WDWrapper();
        }
        return wd;
    }

    private WebDriver driver;
    private WebDriverWait wait;

    private WDWrapper(){
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();

        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("excludeSwitches",
                Collections.singletonList("enable-automation"));

        driver = new ChromeDriver(options);
        wait = new WebDriverWait(driver, Duration.ofMillis(5000), Duration.ofMillis(250));
        driver.manage().window().maximize();
    }

    @Override
    public void get(String url) {
        LOG.debug("Открываю страницу по адресу: " + url);
        driver.get(url);
        LOG.debug("Открываю страницу по адресу: " + url);
    }

    @Override
    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    @Override
    public String getTitle() {
        return driver.getTitle();
    }

    @Override
    public List<WebElement> findElements(By by) {
        List<WebElement> elements;
        try {
            LOG.debug("Ищу элементы по локатору: {}", by);
            // elements = driver.findElements(by);
            elements = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));

            highlightElementByJS("rgb(200,200,255)", elements.toArray(WebElement[]::new));
            LOG.debug("Найдены элементы по локатору: {}", by);
        } catch (NoSuchElementException e) {
            takeScreenshot();
            throw e;
        }
        return elements;
    }


    @Override
    public WebElement findElement(By by) {
        WebElement element;
        try {
            LOG.debug("Ищу элемент по локатору: {}", by);
            // element = driver.findElement(by);
            element = wait.until(ExpectedConditions.presenceOfElementLocated(by));

            highlightElementByJS("rgb(200,200,255)",element);
            LOG.debug("Найден элемент по локатору: {}", by);
        } catch (NoSuchElementException e) {
            takeScreenshot();
            throw e;
        }
        return element;
    }



    @Override
    public String getPageSource() {
        return driver.getPageSource();
    }

    @Override
    public void close() {
        LOG.debug("Закрываю текущую вкладку.");
        driver.close();
    }

    @Override
    public void quit() {
        LOG.debug("Закрываю браузер.");
        driver.quit();
    }

    @Override
    public Set<String> getWindowHandles() {
        return driver.getWindowHandles();
    }

    @Override
    public String getWindowHandle() {
        return driver.getWindowHandle();
    }

    @Override
    public TargetLocator switchTo() {
        return driver.switchTo();
    }

    @Override
    public Navigation navigate() {
        return driver.navigate();
    }

    @Override
    public Options manage() {
        return driver.manage();
    }

    public void highlightElementByJS(String color, WebElement... element) {
        ((JavascriptExecutor)driver).executeScript(
                //обращаемся к свойству элемента в дом
                String.format("arguments[0].style.backgroundColor='%s'", color),
                element
        );
    }

    public void scrollElementIntoView(WebElement element) {
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true)", element);
    }

    /**
     * Клик через JavaScript. Дичайший костыль.
     * Его использование - это роспись в собственной некомпетентности и продажа души сатане.
     *
     * Но иногда без него никак }:->
     */
    @Deprecated
    public static void clickElementByJS(WebElement element, WebDriver driver) {
        ((JavascriptExecutor)driver).executeScript("arguments[0].click()", element);
    }

    public void takeScreenshot() {
        File source = ((RemoteWebDriver)driver).getScreenshotAs(OutputType.FILE);
        try {

            //создаем директорию для скриншотов (если ее еще нет)
            File theDir = new File("screenshot");
            if (!theDir.exists()) {
                theDir.mkdirs();
            }

            //сохраняем туда сам скриншот
            String fileSuffix = new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());
            FileHandler.copy(source, new File("screenshot/" + fileSuffix + "-scr.png"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
