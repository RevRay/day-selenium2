package ru.smlab.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.time.Duration;

public class WaitDemoTest {
    @Test
    public void waitTest() throws InterruptedException {
        //selenium wait - implicit (1) неявные & explicit (2) явные

        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        ChromeDriver driver = new ChromeDriver();
//        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(7000));
        //implicit wait - presence of element in DOM
        //works for findElement()

        driver.get("https://the-internet.herokuapp.com/dynamic_loading/1");

        WebElement startButton = driver.findElement(By.xpath("//div[@id='start']/button"));
        startButton.click();
        WebElement element = driver.findElement(By.xpath("//div[@id='finish']/h4"));

        //explicit wait
        WebDriverWait driverWait = new WebDriverWait(driver, Duration.ofMillis(10000), Duration.ofMillis(100));
//        driverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath()));

        element.click();
//        element.getText();
    }
    @Test
    public void waitTest2() throws InterruptedException {

        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        ChromeDriver driver = new ChromeDriver();

        WebDriverWait driverWait = new WebDriverWait(driver, Duration.ofMillis(10000), Duration.ofMillis(100));

        driver.get("https://the-internet.herokuapp.com/dynamic_loading/1");

        WebElement startButton = driverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='start']/button")));
        startButton.click();

        WebElement element = driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='finish']/h4")));

        element.click();
    }

    @Test
    public void test2() {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        ChromeDriver driver = new ChromeDriver();
//        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(2000));
        driver.get("https://opensource-demo.orangehrmlive.com/");

        driver.findElement(By.xpath("//input[@name='txtUsername']")).sendKeys("Admin");
        driver.findElement(By.xpath("//input[@name='txtPassword']")).sendKeys("admin123");
        driver.findElement(By.xpath("//input[@id='btnLogin']")).click();

        WebElement canvas = driver.findElement(By.xpath("//div[@id='div_graph_display_emp_distribution']/canvas"));
        canvas.click();
    }

}
