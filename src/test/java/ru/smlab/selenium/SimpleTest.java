package ru.smlab.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleTest {
    @Test
    public void simpleTest() {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        ChromeDriver driver = new ChromeDriver();
        driver.get("https://www.yandex.ru");

        WebElement searchInput = driver.findElement(By.xpath("//input[@id='text']")); //DOM #1

        driver.navigate().refresh(); //DOM #2

        searchInput.sendKeys("search query");
    }

    @Test
    public void testScreenshot() {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        ChromeDriver driver = new ChromeDriver();

        driver.get("https://yandex.ru");

        File source = ((RemoteWebDriver)driver).getScreenshotAs(OutputType.FILE);
        try {
            String fileSuffix = new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());
            FileHandler.copy(source, new File(fileSuffix + "-scr.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
