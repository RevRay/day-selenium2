package ru.smlab.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Set;

public class SearchResultPage extends BasePage{

    String searchResultsXpath = "//li[contains(@class,'serp-item')]//a";
    String searchResultXpathPattern = "//li[contains(@class,'serp-item')][%s]//a";
    String searchResultsPagesXpath = "//a[contains(@class,'pager__item')]";
    String searchResultsPageXpathPattern = "//a[contains(@class,'pager__item')][text()='%s']";

    public SearchResultPage() {
        this.url = "https://yandex.ru/search";
    }

    public int getCountOfSearchResults() {
        List<WebElement> searchResults = driver.findElements(By.xpath(searchResultsXpath));
        int searchResultsCount = searchResults.size();
        LOG.info("Получил количество поисковых результатов на текущей странице: {}", searchResultsCount);
        return searchResultsCount;
    }

    public int getCountOfSearchPages() {
        List<WebElement> searchPages = driver.findElements(By.xpath(searchResultsPagesXpath));
        int searchPagesCount = searchPages.size();
        LOG.info("Получил количество кнопок доступных поисковых страниц: {}", searchPagesCount);
        return searchPagesCount;
    }

    public void goToSearchResultsPageNumber(String number) {
        String locator = String.format(searchResultsPageXpathPattern, number);
        System.out.println(locator);
        WebElement resultPageButton = driver.findElement(By.xpath(String.format(searchResultsPageXpathPattern, number)));
        resultPageButton.click();
        LOG.info("Перешел на странице поисковых результатов номер: {}", number);
    }

    public void clickSearchResult(int searchResultNumber) {
        WebElement element = driver.findElement(By.xpath(String.format(searchResultXpathPattern, searchResultNumber)));
        element.click();
        LOG.info("Кликнул по поисковому результату номер {}", searchResultNumber);
    }

    /**
     * Поскольку в результате клика открывается новая вкладка - на нее нужно переключиться
     * (чтобы драйвер начал работать с контекстом новой страницы)
     * @param searchResultNumber
     */
    public void clickSearchResultAndSwitchToResultTab(int searchResultNumber) {
        //определяем список вкладок/окон браузера до клика
        Set<String> handles = driver.getWindowHandles();

        //клик (приводит к открытию поискового результата в новой вкладке)
        clickSearchResult(searchResultNumber);

        //определяем список вкладок/окон браузера после клика
        Set<String> newHandles = driver.getWindowHandles();
        newHandles.removeAll(handles); //оставляем только id новенькой вкладки
        //и переключаем на нее фокус и контекст драйвера
        driver.switchTo().window(newHandles.toArray(String[]::new)[0]);
    }

}
