package ru.smlab.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class MainPage extends BasePage{

    public String marketMenuItemLogoXpathPattern = "//a[@data-id='%s']/div[contains(@class, 'services-new__icon')]";
    public String marketMenuItemTitleXpathPattern = "//a[@data-id='%s']/div[contains(@class, 'services-new__item-title')]";
    public String searchFieldXpath = "//input[@id='text']";
    public String searchButtonXpath = "//button[@type='submit']";

    public enum Menu {
        MARKET("market"),
        VIDEO("video"),
        IMAGES("images"),
        MAPS("maps");

        String name;

        Menu(String name) {
            this.name = name;
        }
    }

    public boolean isMenuItemDisplayed(Menu menuItem) {
        LOG.info("Проверяю отображение логотипа и подписи для пункта меню: {}", menuItem.name);
        WebElement marketMenuItemLogo = driver.findElement(By.xpath(
                String.format(marketMenuItemLogoXpathPattern, menuItem.name))
        );
        WebElement marketMenuItemTitle = driver.findElement(By.xpath(
                String.format(marketMenuItemTitleXpathPattern, menuItem.name))
        );

        return marketMenuItemLogo.isDisplayed() && marketMenuItemTitle.isDisplayed();
    }

    public boolean areAllMenuItemsDisplayed() {
        for (Menu m : Menu.values()) {
            if (!isMenuItemDisplayed(m)) {
                return false;
            }
        }
        return true;
    }

    public void enterSearchQuery(String searchQuery) {
        LOG.info("Ввожу в поисковую строку значение" + searchQuery);
        driver.findElement(By.xpath(searchFieldXpath)).sendKeys(searchQuery);
        LOG.info("Успешно ввел в поисковую строку значение" + searchQuery);
    }
    public void clickSearchButton() {
        LOG.info("кликаю по кнопке поиска");
        driver.findElement(By.xpath(searchButtonXpath)).click();
        LOG.info("кликнул по кнопке поиска");
    }

    public void search(String searchQuery) {
        enterSearchQuery(searchQuery);
        clickSearchButton();
    }


}
