package ru.smlab.selenium.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import ru.smlab.selenium.util.WDWrapper;

public class BasePage {

    public static Logger LOG = LogManager.getLogger(BasePage.class);
    public static WebDriver driver = WDWrapper.getInstance();

    public String url = "https://yandex.ru";

    public void open() {
        driver.get(url);
    }

    public boolean checkThisIsCurrentPage() {
        String currentUrl = driver.getCurrentUrl();
        LOG.info("Проверяю адрес текущей страницы. Ожидаемый адрес: {}; реальный адрес: {}", url, currentUrl);
        return currentUrl.startsWith(url);
    }

}
