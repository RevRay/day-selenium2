package ru.smlab.selenium;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.smlab.selenium.pages.MainPage;
import ru.smlab.selenium.pages.SearchResultPage;

/**
 * Тестовый сценарий для поисковика Яндекс.
 * 1. Открыть страницу yandex.ru
 * 2. Проверить, отображаются ли над поисковой строкой кнопки
 * "Маркет", "Видео", "Картинки", "Карты" (проверяется наличие элементов логотип + текст).
 * 3. Ввести в поле поиска запрос "porsche 356B 1:18 model"
 * 4. Нажать кнопку найти
 * 5. Проверить, что по данному поисковому запросу получено как минимум два результата
 * 6. Проверить, что по данному поисковому запросу есть как минимум 3 страницы результатов
 * 7. Перейти на 3-ю страницу результатов
 * 8. Проверить, что мы все еще находимся в поисковике Яндекс (URL)
 * 9. Открыть 2-й поисковый результат в выдаче на данной странице
 * 10. Убедиться что произошел переход со страницы поисковика на целевую страницу
 */
public class YandexTest {

    @Test
    public void testSearch() {

        //1. Открыть страницу yandex.ru
        MainPage mainPage = new MainPage();
        mainPage.open();

        //2.Проверить, отображаются ли над поисковой строкой кнопки "Маркет", "Видео", "Картинки", "Карты" (проверяется наличие элементов логотип + текст).

        Assert.assertTrue(mainPage.areAllMenuItemsDisplayed());

        // 3. Ввести в поле поиска запрос "porsche 356B 1:18 model"
        // 4. Нажать кнопку найти
        mainPage.search("porsche 356B 1:18 model");

        // 5. Проверить, что по данному поисковому запросу получено как минимум два результата

        SearchResultPage searchResultPage = new SearchResultPage();

        Assert.assertTrue(
                searchResultPage.getCountOfSearchResults() >= 2,
                "Количество поисковых результатов в выдаче менее 2"
        );

        // 6. Проверить, что по данному поисковому запросу есть как минимум 3 страницы результатов

        Assert.assertTrue(
                searchResultPage.getCountOfSearchPages() >= 3,
                "Количество поисковых результатов в выдаче менее 2"
        );

        // 7. Перейти на 3-ю страницу результатов

        searchResultPage.goToSearchResultsPageNumber("3");

        // 8. Проверить, что мы все еще находимся в поисковике Яндекс (URL)

        Assert.assertTrue(searchResultPage.checkThisIsCurrentPage());

        // 9. Открыть 2-й поисковый результат в выдаче на данной странице

        searchResultPage.clickSearchResultAndSwitchToResultTab(2);

        // 10. Убедиться что произошел переход со страницы поисковика на целевую страницу

        Assert.assertFalse(searchResultPage.checkThisIsCurrentPage());

    }

}
